import Vue from 'vue'
import VueRouter from 'vue-router'
//import Home from '../views/Home.vue'

import Homepage from '../components/Home.vue'
import Reservations from '../components/Reservations.vue'
import Persons from '../components/Persons.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Homepage
  },
  {
    path: '/Reservations',
    name: 'Reservations',
    component: Reservations
  },
  
  {
    path: '/Persons',
    name: 'Persons',    
    component: Persons
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
